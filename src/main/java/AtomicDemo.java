

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicDemo {
    private volatile int  volatileCounter = 0;
    private AtomicInteger atomicCounter = new AtomicInteger(0);

    public void incrementVolatile() {
        volatileCounter++;
    }

    public void incrementAtomic() {
        atomicCounter.getAndIncrement();
    }


    @Test
    public void testVolatileCounter() throws InterruptedException {
        //volatile
        Thread t1 = new Thread(() -> incrementVolatile());
        Thread t2 = new Thread(() -> incrementVolatile());
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        Assertions.assertEquals(2, volatileCounter);
    }

    @Test
    public void testAtomicCounter() throws InterruptedException {
        //volatile
        Thread t1 = new Thread(() -> incrementAtomic());
        Thread t2 = new Thread(() -> incrementAtomic());
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        Assertions.assertEquals(2, atomicCounter.get());
    }
}