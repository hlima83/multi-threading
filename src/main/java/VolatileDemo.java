public class VolatileDemo {
    private int counter = 0;
    private volatile int volatileCounter = 0;

    public int getCounter(boolean volat) {
        if (volat)
            return volatileCounter;
        else
            return counter;
    }

    public int getCounterAndIncrement(boolean volat) {
        if (volat)
            return volatileCounter++;
        else
            return counter ++;
    }

    private Runnable createTask1(boolean volat){
        Runnable aRunnable = () -> runTask1(volat);
        return aRunnable;
    }

    private Runnable createTask2(boolean volat){
        Runnable aRunnable = () -> runTask2(volat);
        return aRunnable;
    }

    private void runTask1(boolean volat) {
        int i = 0;
        while (i < 5) {
            System.out.println("Thread " + Thread.currentThread().getName() + ": value " + i++);
        }
        // changing counter
        System.out.println("Thread " + Thread.currentThread().getName() + ": counter incremented " + getCounterAndIncrement(volat));
    }

    private void runTask2(boolean volat) {
        int i = 0;
        while (getCounter(volat) < 1){
            i++;
        }
        System.out.println("Thread " + Thread.currentThread().getName() + ": counter unblocked");
    }

    public static void main(String[] args) {

        VolatileDemo volatileDemo = new VolatileDemo();

        //threads memory
        //task 1
        Runnable task1 = volatileDemo.createTask1(false);
        //task 2
        Runnable task2 = volatileDemo.createTask2(false);
        //main memory
        //task 1
        Runnable task1vol = volatileDemo.createTask1(true);
        //task 2
        Runnable task2vol = volatileDemo.createTask2(true);

        try {
            new Thread(task1).start();
            new Thread(task2).start();

            Thread.sleep(2000);

            new Thread(task1vol).start();
            new Thread(task2vol).start();

            Thread.sleep(2000);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}